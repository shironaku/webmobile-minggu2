void main(List<String> args) {
  // if else
  var putra = true;

  if (putra) {
    print("benar");
  } else {
    print("salah");
  }
  print("==========================");

  // tenary operator
  var dwi = false;
  dwi ? print("benar") : print("salah");
  print("==========================");

  if (true) {
    print("jalankan code");
  }

  if (false) {
    print("Program tidak jalan code");
  }
  print("==========================");

  var mood = "happy";
  if (mood == "happy") {
    print("hari ini aku bahagia!");
  }
  print("==========================");

  // branching
  var alfasmartStatus = "open";
  if (alfasmartStatus == "open") {
    print("saya akan membeli minyak");
  } else {
    print("alfamartnya tutup");
  }
  print("==========================");

  // branching dengan kondisi
  var indahmartStatus = "close";
  var minuteRemainingToOpen = 5;
  if (indahmartStatus == "open") {
    print("saya mau membeli minuman dingin");
  } else if (minuteRemainingToOpen <= 5) {
    print("indahmart akan buka sebentar lagi, saya tungguin");
  } else {
    print("indahmart tutup, saya pulang aja");
  }
  print("==========================");

  // conditional bersarang
  var minimarketStatus = "open";
  var eskrim = "soldout";
  var buah = "soldout";
  if (minimarketStatus == "open") {
    print("saya akan membeli eskrim dan buah");
    if (eskrim == "soldout" || buah == "soldout") {
      print("belanjaan saya tidak lengkap");
    } else if (eskrim == "soldout") {
      print("eskrim habis");
    } else if (buah == "soldout") {
      print("buah habis");
    }
  } else {
    print("minimarket tutup, saya pulang lagi");
  }
  print("==========================");

  // conditional dengan switch case
  var buttonPushed = 3;
  switch (buttonPushed) {
    case 1:
      {
        print('matikan TV!');
        break;
      }
    case 2:
      {
        print('turunkan volume TV!');
        break;
      }
    case 3:
      {
        print('tingkatkan volume TV!');
        break;
      }
    case 4:
      {
        print('matikan suara TV!');
        break;
      }
    default:
      {
        print('Tidak terjadi apa-apa');
      }
  }
}
